package Auftrag3;

/*
    {
		"name": "OSZIMT Buchhandlung",
		"tel": "030-225027-800",
		"fax": "030-225027-809",
		"adresse": {
			"strasse": "Haarlemer Straße",
			"hausnummer": "23-27",
			"plz": "12359",
			"ort": "Berlin"
		}
	}

*/
import java.util.*;
import java.io.*;
import javax.json.*;
import javax.json.spi.*;

import Auftrag2.Buch;

public class WriteBookstoreDataJason {

	public static void main(String[] args) {
		
	//	Buch b1 = new Buch("Java ist auch eine Insel", 1998, 29.95, "Christian Ullenboom");

	   // add your code here
		JsonObjectBuilder haendler = Json.createObjectBuilder();
		haendler.add("name", "OSZIMT Buchhandlung");
		haendler.add("tel", "030-2250027-800");
		haendler.add("fax", "030-2250027-809");
		
		
		JsonObjectBuilder adresse = Json.createObjectBuilder();
		adresse.add("strasse", "Haarlemer Strasse");
		adresse.add("hausnummer", "23-27");
		adresse.add("plz", "12359");
		adresse.add("ort", "Berlin");
		
		haendler.add("adresse", adresse );
		
		JsonObject jo = haendler.build();

		try {
			FileWriter fw = new FileWriter("bookuebung2.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			fw.close();
			jw.close();
		}
		
		catch (Exception ex) {
			ex.printStackTrace();		
		}
	}

}
