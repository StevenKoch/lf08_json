import java.util.*;

public class Buch {
	
	private String titel;
	private ArrayList <String> aListe;

	public Buch(String titel, ArrayList <String> aListe) {
		this.titel = titel;
		this.aListe = aListe;
	}
	public String getTitel() {
		return this.titel;
	}
	
	public ArrayList <String> getaListe() {
		return this.aListe ;
	}

}
